<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    //
    public function register()
    {
        return view('form');
    }

    public function welcome(Request $request)
    {
        //dd($request->all());
        $name = $request['fname'];
        $lname = $request['lname'];
        return view('welcome', ['name' => $name, 'lname' => $lname]);
    }
}
