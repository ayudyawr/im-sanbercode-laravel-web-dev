<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="post">
        @csrf
        <label for="firstname">First name:</label><br><br>
        <input type="text" id="firstname" name="fname"><br><br>
        <label for="lastname">Last name:</label><br><br>
        <input type="text" id="lastname" name="lname"><br><br>
        <label for="gender">Gender:</label><br><br>
        <input type="radio" name="gender" id="m" value="male">
        <label for="m">Male</label><br>
        <input type="radio" name="gender" id="f" value="female">
        <label for="f">Female</label><br>
        <input type="radio" name="gender" id="o" value="other">
        <label for="o">Other</label><br><br>
        <label for="nationality">Nationality:</label><br><br>
        <select name="nationality" id="nationality">
            <option value="indonesia">Indonesia</option>
            <option value="singapore">Singapore</option>
            <option value="malaysia">Malaysia</option>
            <option value="thailand">Thailand</option>
            <option value="autralia">Australia</option>
        </select><br><br>
        <label for="language">Language Spoken:</label><br><br>
        <input type="checkbox" name="language" id="indonesia" value="indonesia">
        <label for="indonesia">Bahasa Indonsia</label><br>
        <input type="checkbox" name="language" id="english" value="english">
        <label for="english">English</label><br>
        <input type="checkbox" name="language" id="other" value="other">
        <label for="other">Other</label><br><br>
        <label for="bio">Bio:</label><br><br>
        <textarea name="bio" id="bio" cols="30" rows="10"></textarea><br>
        <input type="submit" value="Signup">
    </form>

</body>

</html>